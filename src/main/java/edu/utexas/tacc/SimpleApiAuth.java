/**
 *
 */
package edu.utexas.tacc;

import java.util.Map;
import java.util.ResourceBundle;

import org.apache.http.HttpHost;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.security.auth.AuthException;
import com.liferay.portal.security.auth.Authenticator;

/**
 * @author mrhanlon
 *
 */
public class SimpleApiAuth implements Authenticator {

  private static final Log _log = LogFactoryUtil.getLog(SimpleApiAuth.class);

  @Override
  public int authenticateByEmailAddress(long companyId, String emailAddress,
      String password, Map<String, String[]> headerMap,
      Map<String, String[]> parameterMap) throws AuthException {
    _log.error("Authentication by email address not allowed");
    return Authenticator.FAILURE;
  }

  @Override
  public int authenticateByScreenName(long companyId, String screenName,
      String password, Map<String, String[]> headerMap,
      Map<String, String[]> parameterMap) throws AuthException {
    return authenticate(companyId, screenName, password);
  }

  @Override
  public int authenticateByUserId(long companyId, long userId,
      String password, Map<String, String[]> headerMap,
      Map<String, String[]> parameterMap) throws AuthException {
    _log.error("Authentication by user ID not allowed");
    return Authenticator.FAILURE;
  }

  public static int authenticate(long companyId, String username, String password) {
    int authResponse = Authenticator.DNE;

    try {
      Executor exec = Executor.newInstance()
        .auth(new HttpHost(apiHost()), username, password);

      String jsonResp = exec.execute(Request.Post(apiBaseURI() + "/tokens/v1"))
        .returnContent().asString();
      _log.info("Auth response: " + jsonResp);

       JsonNode node = new ObjectMapper().readTree(jsonResp);
       String status = node.get("status").asText();
       if (status.toLowerCase().equals("success")) {
         JsonNode token = node.get("result").get(0).get("token");
         if (token != null) {
           return Authenticator.SUCCESS;
         }
       }
    } catch (Exception e) {
      _log.error(e);
    }

    return authResponse;
  }

  private static String apiHost() {
    ResourceBundle bundle = ResourceBundle.getBundle("api");
    return bundle.getString("xsede.api.host");
  }

  private static String apiBaseURI() {
    ResourceBundle bundle = ResourceBundle.getBundle("api");
    String apiBase = bundle.getString("xsede.api.scheme")
        + bundle.getString("xsede.api.host") + ":"
        + bundle.getString("xsede.api.port")
        + bundle.getString("xsede.api.root");
    return apiBase;
  }

}
